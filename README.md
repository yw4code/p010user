## Deploy
### migration
````
./cmd/artisan migrate up
./cmd/artisan migrate down
````

## Used Packages
Item  | package
---- | --- 
ORM |  gorm 
Dependency Injection | dig
test | testify, go-mock
config | viper